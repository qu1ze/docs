var express = require('express')
  , http = require('http')
  , path = require('path')
  , config = require('./config')
  , mongodb = require('mongodb')
  , MongoStore = require('connect-mongo')(express)
  , MongoClient = mongodb.MongoClient;   
  
MongoClient.connect(config.get('db:connectionString'), function(mongoConnectionError, db) {
	if (mongoConnectionError) throw mongoConnectionError;
	
	var app = express();
	app.set('mongoDb', db);
	
	global.mongoDb = db;	
	
	app.set('port', process.env.PORT || config.get('app:port'));
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'html');
	app.engine('html', require('ejs-locals-improved'));	
	
	//installing all necessary variables for layout
	app.use(function(req, res, next) {
		res.locals.title = config.get('app:title');
		next();
	});
	
	app.use(express.favicon(path.join(__dirname, 'public', 'favicon.ico')));
	app.use(express.logger('dev'));
	app.use(express.json()); // parse json (application/json content type in request) to req.body
	app.use(express.urlencoded()); //parse post data from the client
	app.use(express.cookieParser('your secret here'));
		
	app.use(express.session({
		secret : 'secret_session_phrase',
		cookie : {
			maxAge : config.get('app:cookie:maxAge')
		},
		store : new MongoStore({
			db : config.get('db:name')
		})
	}));
	
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));

	if ('development' == app.get('env')) {
	  app.use(express.errorHandler());
	}

	require('./components/Bootstrap.js')(app);

	http.createServer(app).listen(app.get('port'), function(){
	  console.log('Express server listening on port ' + app.get('port'));
	});
	
});



