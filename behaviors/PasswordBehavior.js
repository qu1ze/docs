var util = require('util')
  , underscore = require('underscore')
  , ModelError = require('../errors/ModelError.js')
  , crypto = require('crypto')
  , superClass = require('../components/Behavior.js');

function PasswordBehavior(model, options) {
	superClass.call(this, arguments);
	
	this._plainPassword = null;	
	
	for (var opt_it in options)
		this[opt_it] = options[opt_it];
	
	PasswordBehavior._instance = this;	
}

util.inherits(PasswordBehavior, superClass);

PasswordBehavior.prototype.passwordAttribute = 'password';
PasswordBehavior.prototype.repeatPasswordAttribute = 'repeatpassword';

PasswordBehavior.getInstance = function() {
	
	if (underscore.isNull(PasswordBehavior._instance))	
		PasswordBehavior._instance = new PasswordBehavior(null);
				
	return PasswordBehavior._instance;
}

PasswordBehavior.prototype.hashPassword = function(password, salt) {	
	return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

PasswordBehavior.prototype.beforeValidate = function() {			
	
	var bInstance = PasswordBehavior.getInstance();	
	
	if (!this.attributes[bInstance.passwordAttribute])
		return;

	if (this.attributes[bInstance.passwordAttribute] !== this.attributes[bInstance.repeatPasswordAttribute])
		throw new ModelError('Passwords are not the same');
		
	bInstance._plainPassword = this.attributes[bInstance.passwordAttribute];
	delete this.attributes[bInstance.passwordAttribute];	
	delete this.attributes[bInstance.repeatPasswordAttribute];	
	
	this.attributes.salt = Math.random() + '';
	this.attributes.hashPassword = bInstance.hashPassword(bInstance._plainPassword, this.attributes.salt);	
}

PasswordBehavior.prototype.afterFind = function(model, result) {
	
	if (underscore.isEmpty(result))
		return;
	
	var bInstance = PasswordBehavior.getInstance();
		
	result.checkPassword = function(password) {
		return bInstance.hashPassword(password, result.salt) === result.hashPassword;
	}	
}

module.exports = PasswordBehavior;

