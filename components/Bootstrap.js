var express = require('express')  
  , fs = require('fs');
	
module.exports = function(application) {	
	
	fs.readdirSync(__dirname + '/../controllers').forEach(function(name) {		
		var controller = require('./../controllers/' + name)
			, name = controller.name || name
			, view_folder_name = name.toLowerCase().substring(0, name.indexOf('Controller'))
			, prefix = controller.prefix || ''
			, app = express()
			, method
			, path;			
			
		app.set('views', __dirname + '/../views/' + view_folder_name);					
			
		for (var key in controller)
		{						
			//ignoring controller ignored routes
			if (controller.ignoredRoutes && ~controller.ignoredRoutes.indexOf(key))
				continue;					
			
			// attaching controller action callbacks
			if (typeof controller[key] == 'function' && ~key.indexOf('action'))
			{
				var path = controller.uriName + '/' + key.substring(6).toLowerCase();
				
				// working with aliases
				if (controller.routerAliases && controller.routerAliases[path])
				{
					app.all(controller.routerAliases[path], controller.filters, controller[key]);
				}
				
				app.all(path, controller.filters, controller[key]);
			}
		}
				
		application.use(app);
	});		
}