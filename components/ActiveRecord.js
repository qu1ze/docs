var superClass = require('./Model.js')
  , ModelError = require('../errors/ModelError.js')
  , mongo = require('mongodb')
  , underscore = require('underscore')
  , util = require('util');

function ActiveRecord()
{
	superClass.call(this, arguments);	
}

util.inherits(ActiveRecord, superClass);

ActiveRecord._models = [];

ActiveRecord.model = function(modelName) {
	
	if (ActiveRecord._models[modelName])	
		return ActiveRecord._models[modelName];	
	else	
		return ActiveRecord._models[modelName] = new modelName;		 
}

ActiveRecord.prototype.tableName = function() {
	return null;
}

ActiveRecord.prototype.find = function(attributes, callback, options) {		
	
}

ActiveRecord.prototype.findAll = function(callback, options) {
	
	var self = this
	  , options = options || {};
	  
	this.getConnection()
		.collection(this.tableName())
		.find({}, options)		
		.toArray(function(err, results) {
			if (err) throw err;
			
			self.afterFind(results);
			
			callback(results);			
		});
}

ActiveRecord.prototype.findByAttributes = function(attributes, callback, options) {
		
	var self = this
	  , options = options || {};
	
	this.getConnection()
		.collection(this.tableName())
		.findOne(attributes, options, function(err, result) {
			if (err) throw err;					
			
			self.setAttributes(result);
			
			self.afterFind(result);
			
			callback(result);			
		});				
}

ActiveRecord.prototype.findAllByAttributes = function(attributes, callback, options) {
	
	var self = this
	  , options = options || {};
	
	this.getConnection()
		.collection(this.tableName())
		.find(attributes, options)		
		.toArray(function(err, results) {
			if (err) throw err;
			
			self.afterFind(results);
			
			callback(results);			
		});
}

ActiveRecord.prototype.findByPk = function(key, callback, options) {
	if (typeof key == 'string')
		this.findByAttributes({_id : mongo.BSONPure.ObjectID.createFromHexString(key)}, callback, options);
	else
		this.findByAttributes({_id : key}, callback, options);
}

ActiveRecord.prototype.afterFind = function afterFind(result) {
	if (underscore.isEmpty(result))
		return;
		
	try {
		this.emit(arguments.callee.name, this, result);	
		
		//assigning the instance				
		result._instance = result instanceof Array ? new ActiveRecord : this;
			
	} catch (err) {
		if (err instanceof ModelError)
			console.log(err.message, err.stack, err.name);
		else
			console.log(err.message, err.stack, err.name); 
	}	
}

ActiveRecord.prototype.aggregate = function(callback, data, options) {
	
	var data = data || {}
	  , options = options || [];	  
	
	this.getConnection()
		.collection(this.tableName())
		.aggregate(data, options, function(err, results) {
			if (err) throw err;
			
			callback(results);
		});
}

/**
 *  @brief Dont' use this method for now, not recommended for efficiency.
 */
ActiveRecord.prototype.pureSave = function(callback, options) {
	// This array can be an array of user objects
	var data = data || [],
		options = options || {};
	
	if (this.beforeValidate())
		this.getConnection()
			.collection(this.tableName())
			.save(this.attributes, options, function(err, result) {		
				if (err) throw err;
				
				callback(null, result);
			});	
	else
		callback(new Error(this._error));
}

ActiveRecord.prototype.save = function(callback, options, data) {
	// This array can be an array of user objects
	var data = data || [],
		options = options || {};
	
	if (this.attributes['_id'])	
		this.update({_id : this.attributes['_id']}, callback, options, data);
	else
		this.insert(callback, options, data);
}

ActiveRecord.prototype.beforeSave = function() {

}

ActiveRecord.prototype.afterSave = function() {

}

ActiveRecord.prototype.update = function(selector, callback, options) {
	
	if (this.beforeValidate())
		this.getConnection()
			.collection(this.tableName())
			.update(selector, this.attributes, options, function(err, result) {		
				if (err) throw err;
				
				callback(null, result);
			});		
	else	
		callback(new Error(this._error));	
}

ActiveRecord.prototype.insert = function(callback, options, data) {
	
	if (this.beforeValidate())
		this.getConnection()
			.collection(this.tableName())
			.insert(data.length ? data : this.attributes, options, function(err, result) {		
				if (err) throw err;
				
				callback(null, result);
			});
	else
		callback(new Error(this._error));
}

module.exports = ActiveRecord;