var eventEmitter = require('events').EventEmitter
  , util = require('util')
  , ModelError = require('../errors/ModelError.js')
  , mongo = require('mongodb')
  , underscore = require('underscore')
  , Behavior = require('../components/Behavior.js');

function Model() {	
	
	var self = this
	  , _connection = typeof mongoDb !== 'undefined' ? mongoDb : null;
	
	this.getConnection = function() {		
		return _connection;
	}
		
	this.attributes = {};

	this._error = '';
	
	if (!underscore.isEmpty(this._behaviors = this.behaviors()))
		for (var ev_it in Behavior.events)
			for (var behavior in this._behaviors)		
				this._behaviors[behavior][Behavior.events[ev_it]] && this.on(Behavior.events[ev_it], this._behaviors[behavior][Behavior.events[ev_it]]);			
		
}

util.inherits(Model, eventEmitter);

Model.prototype.afterValidate = function() {

}

Model.prototype.beforeValidate = function beforeValidate() {	
	try {
		this.emit(arguments.callee.name, this);
		
	} catch (err) {
		if (err instanceof ModelError)
			console.log(err.message, err.stack, err.name);
		else
			console.log(err.message, err.stack, err.name);
		
		this._error = err.message;
		
		return false;
	}	
	
	return true;
}

Model.prototype.getErrors = function() {

}

Model.prototype.addError = function() {

}

Model.prototype.setAttributes = function(attributes) {		
	for (var fieldName in attributes)
		this.attributes[fieldName] = attributes[fieldName];
}

Model.prototype.behaviors = function() {
	return {};
}

module.exports = Model;