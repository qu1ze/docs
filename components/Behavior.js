function Behavior() {
	
}

Behavior.events = [
	'afterConstruct',
	'beforeFind',
	'afterFind',
	'beforeValidate',
	'afterValidate',
	'beforeSave',
	'afterSave',
	'beforeDelete',
	'afterDelete',
	'beforeCount',
	'afterCount',
];

module.exports = Behavior;