function Controller() {

	// this var will be as a controller segment in the route
	this.uriName = '/';	
	
	/*
	 * These actions will be executed before every router action.
	 * Contents: function references
	 */
	this.filters = [
		this.setUserSession
	];
	
	/*
	 * These routes will be ignored by router
	 * Contents: strings
	 */
	this.ignoredRoutes = [];	
	
	/*
	 *	Router aliases.
	 * 	Example: { 'site/index' : '/' }
	 */
	this.routerAliases = {};
	
			
}

Controller.prototype.setUserSession = function(req, res, next) {	

	if (req.session && req.session.user) {
		res.locals.user = req.session.user;
		
		//modifying the session bc it will not refresh cookie without any modifications :( Don't like this line! BUG!
		req.session._garbage = Date();
		
		req.session.touch();		
	}
		
	next();
}

module.exports = Controller;