function subscribe()
{
	var xhr = new XMLHttpRequest;
	
	xhr.open('GET', '/transaction/subscribe', true);
	
	xhr.onload = function() {
		var data = JSON.parse(this.responseText)
		  , new_balance_amount = parseFloat($('#balance_amount').text())
		  , new_won_debit_amount = parseFloat($('#won_debit_amount').text())
		  , new_won_credit_amount = parseFloat($('#won_credit_amount').text())
		  , new_debit_amount = parseFloat($('#debit_amount').text())
		  , new_credit_amount = parseFloat($('#credit_amount').text());
		  
		for (var it = 0; it < data.data.transactions.length; it++)
		{
			var amount = parseFloat(data.data.transactions[it].attributes.amount);
			
			switch (data.data.transactions[it].attributes.type)
			{
				case 1:
					new_credit_amount += amount;
					new_balance_amount += amount;	
					break;
				case 2:
					new_debit_amount += amount;
					new_balance_amount -= amount;
					break;
				case 6:
					new_credit_amount += amount;
					new_balance_amount += amount;
					
					new_won_credit_amount += amount;
					break;
				case 7:
					new_debit_amount += amount;
					new_balance_amount -= amount;
					
					new_won_debit_amount += amount;
					break;
			}						
		}
		
		$('#won_credit_amount').text( new_won_credit_amount.toFixed(2) );
		$('#won_debit_amount').text( new_won_debit_amount.toFixed(2) );
		$('#credit_amount').text( new_credit_amount.toFixed(2) );
		$('#debit_amount').text( new_debit_amount.toFixed(2) );
		$('#balance_amount').text( new_balance_amount.toFixed(2) );
		
		if (new_balance_amount > 0)
		{
			$('#get_paid_btn').removeAttr('disabled');
			
			if ((new_won_credit_amount - new_won_debit_amount) <= 0)
				$('#get_won_btn').attr('disabled', 'disabled');
			else
				$('#get_won_btn').removeAttr('disabled');
				
		} else {
			$('#get_won_btn, #get_paid_btn').attr('disabled', 'disabled');					
		}
		
		
		$('.transactions_table_tbody').prepend(data.row);
		
		initTable(true);
		
		subscribe();
	};
	
	xhr.onerror = xhr.onabort = function() {
		setTimeout(subscribe, 5000);
	};
	
	xhr.send(null);
}

subscribe();

function initTable(withArgs)
{
	if (withArgs)
		$('#transactions_table').dataTable({
			aaSorting : [[0, 'desc']],
			bDestroy : true
		});
	else
		$('#transactions_table').dataTable();
}

$(document).ready(function() {
	initTable(true);
});