var util = require('util')
  , underscore = require('underscore')  
  , TransactionModel = require('../models/TransactionModel')  
  , UserModel = require('../models/UserModel')  
  , superClass = require('../components/Controller.js')
  , config = require('nconf')
  , self = null;
  
function TransactionController()
{	
	superClass.call(this, arguments);				
	
	self = this;
	
	this.uriName = '/transaction';
	
	this.routerAliases = underscore.extend({
		'/transaction/index' : '/transactions'
	}, this.routerAliases);	

	this.filters = this.filters.concat([
		this.filterRegisteredUsers
	]);
	
	this.users = [];
	
	this.broadCast = function(data) {		
		
		function buildRows(localData)
		{
			var rows = [];
			
			underscore.map(localData.transactions, function(transaction) {
				var row = '<tr>'
				  , trnAttrs = transaction.attributes;
				
				row += '<td>' + new Date(trnAttrs.date).toDateString() + '</td>';			
				row += '<td>' + TransactionModel.getType(trnAttrs.type) + '</td>';			
				row += '<td>' + trnAttrs.description + '</td>';			
				row += '<td>' + trnAttrs.user.firstname + ' (' + UserModel.getType(trnAttrs.user.type) + ')' + '</td>';			
				row += '<td colspan="2">$' + parseFloat( trnAttrs.amount ).toFixed(2) + '</td>';
				
				row += '</tr>';
				
				rows.push(row);
			});
			
			return rows;
		}
		
		var r_rows = buildRows(data).join('');
		
		underscore.map(self.users, function(clientRes) {
			clientRes.send({
				row : r_rows,
				data : data
			});
		});
		self.users = [];
	}
}

util.inherits(TransactionController, superClass);

TransactionController.prototype.filterRegisteredUsers = function(req, res, next) {
	if (typeof res.locals.user == 'undefined')
		res.send(401, 'Unauthorized');
	else
		next();
}

TransactionController.prototype.actionSubscribe = function(req, res) {
	self.users.push(res);
}

TransactionController.prototype.actionIndex = function(req, res) {			
	
	TransactionModel.model().findAll(function(transactions_results) {
		TransactionModel.model().aggregate(function(credit_results) {						
			TransactionModel.model().aggregate(function(debit_results) {			
				TransactionModel.model().aggregate(function(add_won_results) {			
					TransactionModel.model().aggregate(function(get_won_results) {			
								
							res.render('index', { 					
								isLogged : !!req.session.user,		
								transactions : transactions_results,
								TransactionModel : TransactionModel,			
								UserModel : UserModel,	
								credit : credit_results.length ? credit_results[0]['sum'] : 0,
								debit : debit_results.length ? debit_results[0]['sum'] : 0,
								add_won : add_won_results.length ? add_won_results[0]['sum'] : 0,
								get_won : get_won_results.length ? get_won_results[0]['sum'] : 0,
								js : [
									{ path : '/js/jquery.dataTables.min.js' },
									{ path : '/js/transactions.js' },
								],
								css : [
									{ path : '/css/jquery.dataTables.css' },
									{ path : '/css/jquery.dataTables_themeroller.css' },
								]
							});	
					}, [
						{ $match : { type : TransactionModel.TYPE_GET_WON } },
						{
							$group : {
								_id : null,
								sum : { $sum : '$amount' }
							}			
						}				
					]);
				}, [
					{ $match : { type : TransactionModel.TYPE_ADD_WON } },
					{
						$group : {
							_id : null,
							sum : { $sum : '$amount' }
						}			
					}				
				]);
				
			}, [
				{ $match : { type : TransactionModel.TYPE_DEBIT } },
				{
					$group : {
						_id : null,
						sum : { $sum : '$amount' }
					}			
				}				
			]);
		
		}, [
			{ $match : { type : TransactionModel.TYPE_CREDIT } },
			{
				$group : {
					_id : null,
					sum : { $sum : '$amount' }
				}			
			}			
		]);			
	}, {sort : [['date', 'desc']]});		
}

TransactionController.prototype.actionAdd = function(req, res) {
	
	var render = function(err) {		
		res.render('add', { 				
			isLogged : !!req.session.user,									
			validationError : err,
			UserModel : UserModel
		});		
	}
			
	if (!underscore.isEmpty(req.body))
	{						
		var new_amount = parseFloat(req.body.amount)
		  , transactionType = TransactionModel.TYPE_CREDIT;
		
		if (req.session.user.balance - new_amount < 0 || !req.session.user.balance) {
			render(new Error('You have no money to add!'));
			return;
		}
		
		UserModel.model().findByPk(req.session.user._id, function(userResult) {
		
			if (req.session.user.type == UserModel.TYPE_SYSTEM) {
				userResult._instance.attributes.credit += new_amount;
			} else {
				userResult._instance.attributes.investments += new_amount;				
			}
			userResult._instance.attributes.balance -= new_amount;
						
			userResult._instance.save(function(err, save_result) {
				if (err) throw err;
				
				//if success update the session
				req.session.user = userResult._instance.attributes;				
				
				var transaction = new TransactionModel;
				transaction.setAttributes(underscore.extend(req.body, {
					user : req.session.user,
					date : Date.now(),
					type : transactionType,			
				}));
				transaction.save(function(err, result) {
					if (err) throw err;
					
					self.broadCast({
						user : req.session.user,
						transactions : [ transaction ]
					});
					
					res.redirect('/transactions');
				});
			
			});						
		});
									
	} else
		render();	
}

//only for system
TransactionController.prototype.actionAddWon = function(req, res) {
	
	var render = function(err) {		
		res.render('addwon', { 				
			isLogged : !!req.session.user,									
			validationError : err,
			UserModel : UserModel
		});		
	}
			
	if (!underscore.isEmpty(req.body))
	{						
		var new_amount = parseFloat(req.body.amount);
		
		if (req.session.user.balance - new_amount < 0 || !req.session.user.balance) {
			render(new Error('You have no money to add!'));
			return;
		}
		
		UserModel.model().findByPk(req.session.user._id, function(userResult) {
		
			
			userResult._instance.attributes.credit += new_amount;			
			userResult._instance.attributes.balance -= new_amount;
						
			userResult._instance.save(function(err, result) {
				if (err) throw err;
				
				//if success update the session
				req.session.user = userResult._instance.attributes;				
				
				var transaction = new TransactionModel;
				transaction.setAttributes(underscore.extend(req.body, {
					user : req.session.user,
					date : Date.now(),
					type : TransactionModel.TYPE_ADD_WON,			
				}));
				transaction.save(function(err, result) {
					if (err) throw err;
					
					self.broadCast({
						user : req.session.user,
						transactions : [ transaction ]
					});
					
					res.redirect('/transactions');
				});
			
			});						
		});
									
	} else
		render();	
}

TransactionController.prototype.actionGetpaid = function(req, res) {
	
	var render = function(err) {
		res.render('getpaid', {
			isLogged : !!req.session.user,
			validationError : err,
			UserModel : UserModel
		});
	}
	
	if (!underscore.isEmpty(req.body))
	{
		var transaction = new TransactionModel
		  , new_amount = parseFloat(req.body.amount)
		  , feed_description = config.get('company:fee:description')
		  , fee = parseFloat( parseFloat(config.get('company:fee:percent')) * new_amount / 100);		  
		
		TransactionModel.model().aggregate(function(credit_results) {
			TransactionModel.model().aggregate(function(debit_results) {
				TransactionModel.model().aggregate(function(won_results) {
			
					var credit = credit_results.length ? credit_results[0]['sum'] : 0
					  , won = won_results.length ? won_results[0]['sum'] : 0
					  , debit = debit_results.length ? debit_results[0]['sum'] : 0;
					  
					if (((won + credit) - debit) - new_amount < 0) {
						render(new Error('There is no such money in the system!'));
						return;
					}
																				
					var bodyData = {
						user : req.session.user,
						date : Date.now(),
						type : TransactionModel.TYPE_DEBIT,
						amount : req.session.user.type == UserModel.TYPE_SYSTEM ? new_amount : new_amount - fee
					};
					
					transaction.setAttributes(underscore.extend(req.body, bodyData));
					transaction.save(function(err, result) {
						if (err) throw err;
						
						if (req.session.user.type == UserModel.TYPE_USER)
						{
							var feed_transaction = new TransactionModel;
							feed_transaction.setAttributes(underscore.extend(bodyData, {
								description : feed_description,
								amount : fee,
								type : TransactionModel.TYPE_FEE
							}));
							feed_transaction.save(function( err, result ) {
								if (err) throw err;
								
								UserModel.model().findByPk(req.session.user._id, function(userResult) {
									
									userResult._instance.attributes.balance += new_amount - fee;
									userResult._instance.attributes.got_paid += new_amount - fee;
									
									userResult._instance.save(function( err, result ) {
										if (err) throw err;
										
										req.session.user = userResult._instance.attributes;								
										
										self.broadCast({
											user : req.session.user,
											transactions : [ transaction, feed_transaction ]
										});
								
										res.redirect('/transactions');
									});							
								});	
								
							});	
						} else {
							
							UserModel.model().findByPk(req.session.user._id, function(userResult) {
									
								userResult._instance.attributes.balance += new_amount;
								userResult._instance.attributes.debit += new_amount;
								
								userResult._instance.save(function( err, result ) {
									if (err) throw err;
									
									req.session.user = userResult._instance.attributes;								
									
									self.broadCast({
										user : req.session.user,
										transactions : [ transaction ]
									});
							
									res.redirect('/transactions');
								});							
							});	
							
						}
					});
				}, [
					{ $match : { type : TransactionModel.TYPE_ADD_WON } },
					{
						$group : {
							_id : null,
							sum : { $sum : '$amount' }
						}			
					}				
				]);
			
			}, [
				{ $match : { type : TransactionModel.TYPE_DEBIT } },
				{
					$group : {
						_id : null,
						sum : { $sum : '$amount' }
					}			
				}				
			]);
		}, [
			{ $match : { type : TransactionModel.TYPE_CREDIT } },
			{
				$group : {
					_id : null,
					sum : { $sum : '$amount' }
				}			
			}				
		]);		
		
	} else
		render();
	
}


TransactionController.prototype.actionGetWon = function(req, res) {
	
	var render = function(err) {
		res.render('getwon', {
			isLogged : !!req.session.user,
			validationError : err,
			UserModel : UserModel
		});
	}
	
	if (!underscore.isEmpty(req.body))
	{
		var transaction = new TransactionModel
		  , new_amount = parseFloat(req.body.amount);
		  	 			
			TransactionModel.model().aggregate(function(add_won_results) {
				TransactionModel.model().aggregate(function(get_won_results) {
			
					var add_won = add_won_results.length ? add_won_results[0]['sum'] : 0
					  , get_won = get_won_results.length ? get_won_results[0]['sum'] : 0;
					  
					if ((add_won - get_won) < 0) {
						render(new Error('There is no WON money in the system!'));
						return;
					}
																				
					var bodyData = {
						user : req.session.user,
						date : Date.now(),
						type : TransactionModel.TYPE_GET_WON,
						amount : new_amount
					};
					
					transaction.setAttributes(underscore.extend(req.body, bodyData));
					transaction.save(function(err, result) {
						if (err) throw err;
																				
							UserModel.model().findByPk(req.session.user._id, function(userResult) {
									
								userResult._instance.attributes.balance += new_amount;
								userResult._instance.attributes.investments -= new_amount;
								
								userResult._instance.save(function( err, result ) {
									
									req.session.user = userResult._instance.attributes;								
									
									self.broadCast({
										user : req.session.user,
										transactions : [ transaction ]
									});
							
									res.redirect('/transactions');
								});							
							});																				
					});
				}, [
					{ $match : { type : TransactionModel.TYPE_GET_WON } },
					{
						$group : {
							_id : null,
							sum : { $sum : '$amount' }
						}			
					}				
				]);
			}, [
				{ $match : { type : TransactionModel.TYPE_ADD_WON } },
				{
					$group : {
						_id : null,
						sum : { $sum : '$amount' }
					}			
				}				
			]);

	} else
		render();
	
}


TransactionController.prototype.actionDeposit = function(req, res) {
	
	var render = function(err) {
		res.render('deposit', {
			isLogged : !!req.session.user,
			validationError : err,
			UserModel : UserModel
		});
	}
	
	if (!underscore.isEmpty(req.body))
	{
		var transaction = new TransactionModel
		  , new_amount = parseFloat(req.body.amount);
		  
		transaction.setAttributes(underscore.extend(req.body, {
			user : req.session.user,
			date : Date.now(),
			type : TransactionModel.TYPE_DEPOSIT,			
		}));
		transaction.save(function(err, transactionResult) {
			if (err) throw err;
			
			UserModel.model().findByPk(req.session.user._id, function(userResult) {
				
				userResult._instance.attributes.balance += new_amount;
				
				userResult._instance.save(function( err, result ) {
					if (err) throw err;
										
					req.session.user = userResult._instance.attributes;
					
					self.broadCast({
						user : req.session.user,
						transactions : [ transaction ]
					});
					
					res.redirect('/transactions');
				});							
			});					
		});
	} else
		render();
	
}

TransactionController.prototype.actionWithdraw = function(req, res) {
	
	var render = function(err) {
		res.render('withdraw', {
			isLogged : !!req.session.user,
			validationError : err,
			UserModel : UserModel
		});
	}
	
	if (!underscore.isEmpty(req.body))
	{
		var transaction = new TransactionModel
		  , new_amount = parseFloat(req.body.amount);
		  
		transaction.setAttributes(underscore.extend(req.body, {
			user : req.session.user,
			date : Date.now(),
			type : TransactionModel.TYPE_WITHDRAWAL,			
		}));
		transaction.save(function(err, result) {
			if (err) throw err;
			
			UserModel.model().findByPk(req.session.user._id, function(userResult) {
				
				userResult._instance.attributes.balance -= new_amount;
				userResult._instance.save(function( err, result ) {
					if (err) throw err;
					
					req.session.user = userResult._instance.attributes;
					
					self.broadCast({
						user : req.session.user,
						transactions : [ transaction ]
					});
					
					res.redirect('/transactions');
				});							
			});					
		});
	} else
		render();
	
}

module.exports = new TransactionController();
