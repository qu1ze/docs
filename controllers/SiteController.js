var util = require('util')
  , underscore = require('underscore')
  , UserModel = require('../models/UserModel.js')
  , TransactionModel = require('../models/TransactionModel.js')
  , superClass = require('../components/Controller.js');

function SiteController()
{	
	superClass.call(this, arguments);			
	
	this.uriName = '/site';
	
	this.routerAliases = underscore.extend({
		'/site/index' : '/',
		'/site/login' : '/login',
		'/site/logout' : '/logout',
		'/site/signup' : '/signup',
	}, this.routerAliases);			
	
}

util.inherits(SiteController, superClass);

SiteController.prototype.actionIndex = function(req, res) {			
		
	res.render('index', { 		
		isLogged : !!req.session.user,	
		UserModel : UserModel
	});	
}

SiteController.prototype.actionSignup = function(req, res) {
	
	var scripts = [],
		styles = [];
	
	if (!underscore.isEmpty(req.body))
	{
		var user = new UserModel;
		user.setAttributes(underscore.extend(req.body, { balance : 0, investments : 0, got_paid : 0, credit : 0, debit : 0 }));
		user.save(function(err, result) {
			if (err) 
				res.send(err.message);
			else
				res.redirect('/');
		});
	}
	else		
		res.render('signup', {
			isLogged : !!req.session.user,	
			userTypes : underscore.extend(UserModel.types, { 0 : 'Select user type...' }),	
			css : styles,
			js : scripts,
			UserModel : UserModel
		});	
}

SiteController.prototype.actionLogout = function(req, res) {
	req.session.destroy();
	
	res.redirect('/');
}

SiteController.prototype.actionLogin = function(req, res) {
	
	var req_body = req.body
	  , input_email = req_body.email
	  , input_password = req_body.password;	
	
	UserModel.model().findByAttributes({
		email : input_email,		
	}, function(result) {		
		
		if (underscore.isEmpty(result) || !result.checkPassword(input_password)) {
			res.redirect('/');	
			return;
		}
					
		req.session.user = result;
		res.redirect('/');	
	});	
}

module.exports = new SiteController();
