var util = require('util')
  , underscore = require('underscore')
  , UserModel = require('../models/UserModel.js')
  , superClass = require('../components/Controller.js');

function UserController()
{	
	superClass.call(this, arguments);			
	
	this.uriName = '/user';
	
	this.routerAliases = underscore.extend({
		'/user/index' : '/user'
	}, this.routerAliases);	

	this.filters = this.filters.concat([
		this.filterRegisteredUsers
	]);
	
}

util.inherits(UserController, superClass);

UserController.prototype.filterRegisteredUsers = function(req, res, next) {
	if (typeof res.locals.user == 'undefined')
		res.send(401, 'Unauthorized');
	else
		next();
}

UserController.prototype.actionIndex = function(req, res) {			
		
	res.render('index', { 	
		isLogged : !!req.session.user,		
		UserModel : UserModel		
	});	
}

UserController.prototype.actionEdit = function(req, res) {			
		
	res.render('edit', { 		
		isLogged : !!req.session.user,		
		UserModel : UserModel
	});	
}

UserController.prototype.actionUpdate = function(req, res) {
	
	UserModel.model().findByPk(res.locals.user._id, function(result) {
		if (!underscore.isEmpty(result)) 
		{
			result._instance.setAttributes({
				firstname : req.body.firstname,
				lastname : req.body.lastname
			});
			result._instance.save(function(err, save_result) {
				if (err)
					res.send(err.message);
				else {
					req.session.user = result._instance.attributes;
					res.redirect('/user');					
				}
			});
		}
	});
}

module.exports = new UserController();
