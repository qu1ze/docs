var util = require('util')	
  , underscore = require('underscore')	
  , PasswordBehavior = require('../behaviors/PasswordBehavior.js')
  , superClass = require('../components/ActiveRecord.js');
  
function UserModel() {
	
	superClass.call(this, arguments);	
	
	this.hashedPassword = '';
	this.salt = '';
}

util.inherits(UserModel, superClass);

UserModel.TYPE_USER = 1;
UserModel.TYPE_SYSTEM = 2;

UserModel.types = [];
UserModel.types[UserModel.TYPE_USER] = 'User';
UserModel.types[UserModel.TYPE_SYSTEM] = 'System';

UserModel.getType = function(type) {
	return UserModel.types[type];
}

UserModel.prototype.behaviors = function() {
	return {	
		PasswordBehavior : new PasswordBehavior(this, { 
			passwordAttribute : 'password',
			repeatPasswordAttribute : 'repeatpassword'
		})
	};
}

UserModel.prototype.tableName = function() {
	return 'users';
}

UserModel.model = function() {			
	return UserModel.super_.model(UserModel);
}

UserModel.prototype.beforeValidate = function() {
	superClass.prototype.beforeValidate.call(this);	
	
	this.attributes.balance = parseFloat(this.attributes.balance);		
	
	return true;
}

module.exports = UserModel;