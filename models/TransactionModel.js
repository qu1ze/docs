var util = require('util')	
  , underscore = require('underscore')	  
  , superClass = require('../components/ActiveRecord.js');
  
function TransactionModel() {
	
	superClass.call(this, arguments);	
		
}

util.inherits(TransactionModel, superClass);

TransactionModel.TYPE_CREDIT = 1;

TransactionModel.TYPE_DEBIT = 2;

TransactionModel.TYPE_DEPOSIT = 3;

TransactionModel.TYPE_WITHDRAWAL = 4;

TransactionModel.TYPE_FEE = 5;

TransactionModel.TYPE_ADD_WON = 6;

TransactionModel.TYPE_GET_WON = 7;

TransactionModel.types = [];
TransactionModel.types[TransactionModel.TYPE_CREDIT] = 'Credit';
TransactionModel.types[TransactionModel.TYPE_DEBIT] = 'Debit';
TransactionModel.types[TransactionModel.TYPE_DEPOSIT] = 'Deposit';
TransactionModel.types[TransactionModel.TYPE_WITHDRAWAL] = 'Withdrawal';
TransactionModel.types[TransactionModel.TYPE_FEE] = 'Fee';
TransactionModel.types[TransactionModel.TYPE_ADD_WON] = 'Add Won';
TransactionModel.types[TransactionModel.TYPE_GET_WON] = 'Get Won';

TransactionModel.prototype.tableName = function() {
	return 'transactions';
}

TransactionModel.model = function() {			
	return TransactionModel.super_.model(TransactionModel);
}

TransactionModel.getType = function(type) {
	return TransactionModel.types[type];
}

TransactionModel.prototype.beforeValidate = function() {
	superClass.prototype.beforeValidate.call(this);	
	
	this.attributes.amount = parseFloat(this.attributes.amount);
	
	if (!parseInt(this.attributes.type)) {
		this._error = 'Empty type field';
		return false;
	}
	
	return true;
}

module.exports = TransactionModel;