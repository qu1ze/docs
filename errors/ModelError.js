var util = require('util');

function ModelError(message) {
	this.message = message;
	Error.captureStackTrace(this, ModelError);
}

util.inherits(ModelError, Error);

ModelError.prototype.name = 'ModelError';


module.exports = ModelError;